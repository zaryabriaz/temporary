class Supplier < ApplicationRecord
  has_one :finance_account
  has_many :banks
  has_many :transactions
end
