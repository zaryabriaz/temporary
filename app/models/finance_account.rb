class FinanceAccount < ApplicationRecord
  belongs_to :bank
  belongs_to :customer
  belongs_to :supplier

end
