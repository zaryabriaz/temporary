class CreateAssets < ActiveRecord::Migration[6.0]
  def change
    create_table :assets do |t|
      t.string :name
      t.references :finance_account, null: false, foreign_key: true

      t.timestamps
    end
  end
end
