class CreateSuppliers < ActiveRecord::Migration[6.0]
  def change
    create_table :suppliers do |t|
      t.string :name
      t.text :address
      t.string :ntn
      t.string :cnic
      t.string :email
      t.string :telephone
      t.string :mobile

      t.timestamps
    end
  end
end
