class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.datetime :date
      t.string :payee
      t.text :description
      t.string :type
      t.string :selection
      t.string :ref_no
      t.float :spent_amount
      t.float :received
      t.references :bank, null: false, foreign_key: true
      t.references :customer, null: false, foreign_key: true
      t.references :supplier, null: false, foreign_key: true

      t.timestamps
    end
  end
end
