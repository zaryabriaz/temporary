# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_23_091231) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.integer "resource_id"
    t.string "author_type"
    t.integer "author_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "assets", force: :cascade do |t|
    t.string "name"
    t.integer "finance_account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["finance_account_id"], name: "index_assets_on_finance_account_id"
  end

  create_table "banks", force: :cascade do |t|
    t.string "name"
    t.string "payment_category"
    t.string "default_payment_method"
    t.string "branch_name"
    t.string "branch_code"
    t.string "bank_account_number"
    t.integer "finance_account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["finance_account_id"], name: "index_banks_on_finance_account_id"
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.text "address"
    t.string "ntn"
    t.string "cnic"
    t.string "email"
    t.string "telephone"
    t.string "mobile"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "equities", force: :cascade do |t|
    t.string "name"
    t.integer "finance_account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["finance_account_id"], name: "index_equities_on_finance_account_id"
  end

  create_table "expenses", force: :cascade do |t|
    t.string "name"
    t.integer "finance_account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["finance_account_id"], name: "index_expenses_on_finance_account_id"
  end

  create_table "finance_accounts", force: :cascade do |t|
    t.string "type"
    t.float "opening_balance"
    t.float "credit_limit"
    t.float "credit_amount"
    t.text "description"
    t.integer "bank_id", null: false
    t.integer "customer_id", null: false
    t.integer "supplier_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bank_id"], name: "index_finance_accounts_on_bank_id"
    t.index ["customer_id"], name: "index_finance_accounts_on_customer_id"
    t.index ["supplier_id"], name: "index_finance_accounts_on_supplier_id"
  end

  create_table "incomes", force: :cascade do |t|
    t.string "name"
    t.integer "finance_account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["finance_account_id"], name: "index_incomes_on_finance_account_id"
  end

  create_table "liabilities", force: :cascade do |t|
    t.string "name"
    t.integer "finance_account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["finance_account_id"], name: "index_liabilities_on_finance_account_id"
  end

  create_table "suppliers", force: :cascade do |t|
    t.string "name"
    t.text "address"
    t.string "ntn"
    t.string "cnic"
    t.string "email"
    t.string "telephone"
    t.string "mobile"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.datetime "date"
    t.string "payee"
    t.text "description"
    t.string "type"
    t.string "selection"
    t.string "ref_no"
    t.float "spent_amount"
    t.float "received"
    t.integer "bank_id", null: false
    t.integer "customer_id", null: false
    t.integer "supplier_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bank_id"], name: "index_transactions_on_bank_id"
    t.index ["customer_id"], name: "index_transactions_on_customer_id"
    t.index ["supplier_id"], name: "index_transactions_on_supplier_id"
  end

  add_foreign_key "assets", "finance_accounts"
  add_foreign_key "banks", "finance_accounts"
  add_foreign_key "equities", "finance_accounts"
  add_foreign_key "expenses", "finance_accounts"
  add_foreign_key "finance_accounts", "banks"
  add_foreign_key "finance_accounts", "customers"
  add_foreign_key "finance_accounts", "suppliers"
  add_foreign_key "incomes", "finance_accounts"
  add_foreign_key "liabilities", "finance_accounts"
  add_foreign_key "transactions", "banks"
  add_foreign_key "transactions", "customers"
  add_foreign_key "transactions", "suppliers"
end
